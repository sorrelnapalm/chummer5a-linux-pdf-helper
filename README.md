# Chummer5a Linux PDF helper

you know those links in Chummer where you click on them and it opens the sourcebook in a PDF reader and flips to the correct page?

those links that say things like `SR5, 50` and `SL, 122`.

if you want those links to open in your favorite native Linux PDF reader, this is the script for you!

## not so quick start

this script requires python 3. it relies on only core library modules, so as long as you have python 3 it should work.

first, clone this repository.

`git clone https://gitlab.com/sorrelnapalm/chummer5a-linux-pdf-helper`

open Chummer5a, go to the **global settings**, and set **Location of PDF application** to `helper/cph.py`, which is in the repo you just cloned. also, make the script executable: `chmod +x cph.py`

then, under that, set the **PDF Parameters** to "Web Browser". without this, the script won't work.

the script must be configured before it's usable. create a text file called `cph.ini` in `$HOME/.config/` or `$XDG_CONFIG_HOME/`, or set `$CHUMMER_PDF_HELPER_HOME` and create `cph.ini` in there. this is going to tell the script what PDF reader to use and how to open PDFs in it.

the default configuration is as follows. if no `cph.ini` is found or there is an error while parsing `cph.ini` , the script will follow this. feel free to use this as a starting point for your own `cph.ini`. some argument formats for common Linux PDF viewers are included.

```
[chummer-pdf-helper]
pdf-reader = firefox
run-string = ${pdf-reader} ${web:run-args}
chummer-prefix = ~/.wine

[web]
run-args = file://$$(PDF)$$#page=$$(PAGE)$$

[zathura]
run-args = -P $$(PAGE)$$ $$(PDF)$$

[mupdf]
run-args = $$(PDF)$$ $$(PAGE)$$

[qpdfview]
run-args = $$(PDF)$$#$$(PAGE)$$

[evince]
run-args = -i $$(PAGE)$$ $$(PDF)$$

```

the fields in `cph.ini` are described in the section below.

## cph.ini

there *must* be a section called `chummer-pdf-helper`. a section is denoted by square brackets.

in this section, there *must* be entries called `pdf-reader`, `run-string`, and `chummer-prefix`.

`pdf-reader`: the path to your desired pdf reader. the full path does not need to be specified as long as it is in your `$PATH`.

`run-string`: the full command to launch the pdf reader with arguments specifying the path to the PDF and page number.

`chummer-prefix`: path to the chummer prefix you are using to launch Chummer 5.

this config file is parsed using python3's configparser: https://docs.python.org/3/library/configparser.html.

the script looks for a configuration file in the order below, and parses the first existing file.

1. a path specified as an argument to `cph.py` i.e. `--config-dir custom/path/to/some_config.ini`. useful for debugging but cannot be used when Chummer runs this script.

2. `$CHUMMER_PDF_HELPER_HOME/cph.ini`

3. `$XDG_CONFIG_HOME/cph.ini`

4. `$HOME/.config/cph.ini`

## why is the chummer wineprefix important to specify?

if your shadowrun PDFs are in a folder in `c:\\` in the wineprefix, you will need to provide to the script the path of the wineprefix. if they are in `z:\\` (which WINE maps to `/`) then this path is not needed, because any path in `z:\\` contains the full POSIX path.

take this example:

say we have a wineprefix like `$HOME/chummerpfx` and you launch Chummer in that prefix like so:

`WINEPREFIX=$HOME/chummerpfx wine64 <path-to-Chummer5.exe>`

and you want to access your shadowrun PDFs easily in Chummer so you create a symlink to the folder they're contained in:

`ln -s $HOME/shadowrun-pdfs/ $HOME/chummerpfx/drive_c/shadowrun`

Chummer will see `c:\\shadowrun\\<your PDFs>` and it will try to open PDF readers by giving them `c:\\shadowrun\\Shadowrun_5E.pdf`, for example. but native Linux PDF readers have no idea what `c:\\` is.

`c:\\` is really a folder named `drive_c` in the wineprefix, so we have to convert the drive letter to, for this example, `$HOME/chummerpfx/drive_c/`. this yields the full Linux path to the PDF, `$HOME/chummerpfx/drive_c/shadowrun/Shadowrun_5E.pdf`. that path is provided to the PDF reader specified in `cph.ini`.

## caveats

this script is very messy. very very messy. and possibly brittle. cleanup in progress.

this script is only for Chummer 5 running in WINE.

I have only tested this script using 64-bit wine 7.0, 7.1, and 7.2 staging and Chummer 5.221.0 and 5.222.0. as Chummer gets updated I will try to test with the new versions.

spaces in sourcebook filenames not recommended. seems like some PDF readers like zathura don't like them (the script converts the spaces to %20).

lastly, 

## contributing

feel free to raise issues - I'll get to them as soon as I can.

for some debug output, look in `/tmp/chummer-pdf-helper.log`.
