#!/usr/bin/env python3

import argparse
import configparser
import logging
import os
import pathlib
import string
import subprocess
import sys
import threading
import time
import tkinter
from tkinter import ttk

import config_handler
import cph_logger
import launcher

cph_logger.init_logs()
log = logging.getLogger(__name__)

def parse_args(argv):
    parser = argparse.ArgumentParser(description='Converts windows-style paths into UNIX paths and opens a pdf reader. For use with Chummer5 running in WINE.\nNote that these arguments are controlled by Chummer during normal use.\nCurrently this only supports the "Web Browser" style arguments.')
    parser.add_argument('--config-dir', '-C', default=None, help='Optional custom path to configuration file')
    parser.add_argument('path', help='Windows-style path to the PDF Chummer 5 wants to open')
    parser.add_argument('extra_args', nargs=argparse.REMAINDER, help='Additional args to pass to the PDF reader')

    args = parser.parse_args()

    return args

def convert_win_path(path_str, chummer_pfx):
    log.debug('raw path str: {}'.format(path_str))
    chummer_pfx = str(pathlib.Path(chummer_pfx).expanduser())
    log.debug('pfx: {}'.format(chummer_pfx))
    winpath = None

    # find page number
    page = 1
    idx = path_str.find('#page=')
    if idx != -1:
        page = int(path_str[idx+6:])
        path_str = path_str[:idx]

    if path_str[0] == '/':
        # something is putting the extra / in front of the win path; cut it out
        winpath = pathlib.PureWindowsPath(path_str[1:])
    elif path_str[0] in string.ascii_letters and path_str[1] == ':':
        winpath = pathlib.PureWindowsPath(path_str)
    elif path_str.startswith('file://'):
        path_str = path_str.removeprefix('file://')
        winpath = pathlib.PureWindowsPath(path_str)
    else:
        winpath = None
    log.debug('winpath: {}'.format(winpath))

    try:
        parts = list(winpath.parts)
        drive_letter = winpath.drive[0]
        unix_path = None

        log.debug(parts)

        # if the win path is the Z drive, we find the corresponding unix path
        if drive_letter == 'z' or drive_letter == 'Z':
            parts[0] = '/'
        else:
            if drive_letter in string.ascii_letters:
                # if the win path isn't the Z drive, look in the wineprefix for drive_N directory
                parts[0] = chummer_pfx
                # add the folder drive_<driveletter>
                parts.insert(1, 'drive_' + drive_letter.lower())

        log.debug(parts)
        unix_path_str = '/'.join(parts)

        return (unix_path_str, page)
    except NoneType as err:
        # tried to parse as winpath but came out as None - maybe this isn't a windows path?
        log.exception(err)
        return (path_str, page)

def raise_message_box(title, lines, exit=False, log_function=log.error):
    root = tkinter.Tk()
    frm = ttk.Frame(root, padding=10)
    frm.grid()

    i = 0
    for l in lines:
        ttk.Label(frm, text=l).grid(column=0, row=i)
        i += 1
    ttk.Button(frm, text='Quit', command=root.destroy).grid(column=0, row=i)

    root.title(title)
    root.mainloop()

    log_function(lines)
    if exit:
        sys.exit()

if __name__ == '__main__':
    header = ['\n' * 2] + ['-' * 16] + ['chummer pdf helper'] + ['-' * 16]
    log.debug("".join(header))

    log.debug("argv: {}".format(sys.argv))

    if len(sys.argv) > 2 and sys.argv[1] == '-p' and sys.argv[2] in string.digits:
        # looks like the "unix style" params is selected in Chummer; can't launch; error out
        title = 'Chummer 5 PDF Helper - Parameter Error'
        msg = ['Cannot launch PDF reader using "Unix-style" PDF parameters.',
            'Please use the "Web Browser" parameters in your Chummer 5 global settings.']

        raise_message_box(title, msg, exit=True)

    args = {}
    try:
        args = parse_args(sys.argv)
        print("parsed args", args)
    except Exception as err:
        title = 'Chummer 5 PDF Helper - Error'
        msg = [
            str(err),
        ]
        log.exception(err)
        raise_message_box(title, msg, exit=True)

    cfg = config_handler.read_config(custom_path=args.config_dir)
    pfx = cfg.get('chummer-prefix', '~/.wine')

    try:
        pdf_path, page = convert_win_path(args.path, pfx)
    except Exception as err:
        log.exception(err)
        raise_message_box(title, msg, exit=True)

    log.debug('pdf path is {}, page: {}'.format(pdf_path, page))
    launcher = launcher.PdfReaderLauncher(cfg, pdf_path, page, args.extra_args)

    log.debug('run str: {}'.format(launcher.run_str + launcher.extra_args))

    launcher.launch()

    sys.exit()

'''
    if debug:
        title = 'Chummer 5 PDF Helper - Debug'
        msg = [
            'pdf path: {}'.format(pdf_path),
            'reader path: {}'.format(reader),
            'page: {}'.format(page),
            'arg type: {}'.format(arg_type),
            'custom page arg: {}'.format(custom_page_arg),
            'extra reader args: {}'.format(args.extra_args),
            'pfx: {}'.format(chummer_pfx),
        ]
        # launch msg box in separate thread so it doesn't block pdf opening
        box = threading.Thread(target=raise_message_box, args=(title, msg), kwargs={'exit': False})
        box.start()

    launcher_kwargs = {
        "pdf_path": pdf_path,
        "reader_path": reader_path,
        "reader_args": reader_args,
        "page": page,
    }

    launcher = None

    if debug:
        box.join()
'''
