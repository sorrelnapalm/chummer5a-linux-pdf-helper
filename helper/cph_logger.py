#!/usr/bin/env python3

import logging

def init_logs():
    LOG_FNAME = '/tmp/chummer-pdf-helper.log'

    logging.basicConfig(
        filename=LOG_FNAME,
        level=logging.DEBUG,
        encoding='utf-8',
        format='%(asctime)s:%(name)s:%(lineno)s:%(levelname)s:%(message)s',
    )
