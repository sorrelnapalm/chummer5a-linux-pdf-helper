#!/usr/bin/env python3

import configparser
import logging
import os
import pathlib as pl

log = logging.getLogger(__name__)

CFG_NAME = 'cph.ini'

DEFAULT_CONFIG = """
[chummer-pdf-helper]
pdf-reader = firefox
run-string = ${pdf-reader} ${web:run-args}
chummer-prefix = ~/.wine

[web]
run-args = file://$$(PDF)$$#page=$$(PAGE)$$

[zathura]
run-args = -P $$(PAGE)$$ $$(PDF)$$

[mupdf]
run-args = $$(PDF)$$ $$(PAGE)$$

[qpdfview]
run-args = $$(PDF)$$#$$(PAGE)$$

[evince]
run-args = -i $$(PAGE)$$ $$(PDF)$$

"""

"""
Exception that is raised if get_config_path() cannot find a valid config file.

"""
class NoCphConfigurationFileError(Exception):
    pass

"""
Reads a config file (see get_config_path), or returns DEFAULT_CONFIG if no
config file is found.

Returns a ChummerPdfHelperConfig object.
"""
def read_config(custom_path=None):
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())

    # try finding a config file and reading it
    try:
        pl_path = get_config_path(custom_path=custom_path)
        if pl_path.is_file():
            config.read(pl_path)
    except Exception as err:
        log.exception(err)
        log.warning('using default config')
        config.read_string(DEFAULT_CONFIG)

    # find the right section
    try:
        cfg_section = config['chummer-pdf-helper']
    except Exception as err:
        log.exception(err)
        log.warning('using default config')
        config.read_string(DEFAULT_CONFIG)
        cfg_section = config['chummer-pdf-helper']
    finally:
        for (key, value) in cfg_section.items():
            log.debug('{} = {}'.format(key, value))
        return cfg_section

    return None

"""
Finds a configuration file for the chummer pdf helper in order of:

1. custom_path/CFG_NAME, usually passed to the application as a parameter
2. $CHUMMER_PDF_HELPER_HOME/CFG_NAME
3. $XDG_CONFIG_HOME/CFG_NAME
4. $HOME/.config/CFG_NAME

Returns a pathlib path object if a config file is found, otherwise raises
NoCphConfigurationFileError.

Currently CFG_NAME is 'cph.ini'.
"""
def get_config_path(custom_path=None):
    paths = (
        custom_path,
        os.environ.get('CHUMMER_PDF_HELPER_HOME', None),
        os.environ.get('XDG_CONFIG_HOME', None),
        os.environ.get('HOME', '~/') + '/' + '.config',
    )

    log.debug('Paths: {}'.format(paths))

    if custom_path is None:
        existing_paths = [pl.Path(p) for p in paths if p is not None and os.path.isdir(p)]
    else:
        # only try custom path if one is provided
        custom_pl_path = pl.Path(custom_path)
        if custom_pl_path.is_file():
            return custom_pl_path
        else:
            raise NoCphConfigurationFileError('path to configuration file is not a file!')

    log.debug('existing paths: {}'.format(existing_paths))

    if len(existing_paths) > 0:
        for p in existing_paths:
            pl_path = p / CFG_NAME
            log.debug('checking path: {}'.format(str(pl_path)))
            if pl_path.is_file():
                log.debug('found cfg at: {}'.format(str(pl_path)))
                return pl_path

    error_str = 'No configuration file named {} found in '.format(CFG_NAME)
    error_str += '$CHUMMER_PDF_HELPER_HOME, $XDG_CONFIG_HOME, or $HOME/.config; '
    error_str += 'using defaults.'

    log.warning(error_str)

    raise NoCphConfigurationFileError(error_str)

if __name__ == '__main__':
    read_config()
