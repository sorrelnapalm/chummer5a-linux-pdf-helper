#!/usr/bin/env python3

import pathlib
import subprocess
import urllib

class PdfReaderLauncher(object):
    def __init__(self, config, pdf, page, extra_args):
        self.config = config
        self.extra_args = extra_args

        self._process_config(pdf, page)

    def _process_config(self, pdf, page):
        if self.config is None:
            return

        # build list of args to run using subprocess.run()
        run_str = self.config.get('run-string', '')
        # substitute $(PAGE)$ and $(PDF)$
        page_marker = '$(PAGE)$'
        pdf_marker = '$(PDF)$'
        if pdf_marker in run_str:
            run_str = run_str.replace(pdf_marker, pdf)
        if page_marker in run_str:
            run_str = run_str.replace(page_marker, str(page))

        self.run_str = run_str.split(' ')

    def launch(self):
        subprocess.run(self.run_str + self.extra_args)

